//
//  EditTeamViewController.swift
//  Referee Notes
//
//  Created by Bartek Svaberg on 2016-04-28.
//  Copyright © 2016 Bartek Svaberg. All rights reserved.
//

import UIKit

protocol colorProtocol {
    func colorFromPicker(color : UIColor, identifier : Team) -> ()
}

class EditTeamViewController: UIViewController, HSBColorPickerDelegate {
    
    var match : Match?
    var team : Team?
    var color : UIColor?

    @IBOutlet weak var lblTeamName: UITextField!
    @IBOutlet weak var cpView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Edit \(team!.name)"
        var newFrame = self.cpView.frame
        newFrame.size.width = self.cpView.frame.width / 1.1
        self.color = team?.color
        
        let colorPicker : HSBColorPicker = HSBColorPicker.init(frame: newFrame)
        colorPicker.delegate = self
        self.view.addSubview(colorPicker)


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func HSBColorColorPickerTouched(sender: HSBColorPicker, color: UIColor, point: CGPoint, state: UIGestureRecognizerState) {
        self.color = color
    }
    
    override func viewWillAppear(animated: Bool) {
        self.title = "Edit \(team!.name)"
        self.color = team?.color
        self.lblTeamName.text = team?.name
    }
    
    override func viewWillDisappear(animated: Bool) {
        team?.color = self.color!
        team!.name = self.lblTeamName.text!
        self.team?.color = self.color!
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
