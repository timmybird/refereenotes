//
//  PlayerTableViewCell.swift
//  Referee Notes
//
//  Created by Bartek Svaberg on 2016-04-26.
//  Copyright © 2016 Bartek Svaberg. All rights reserved.
//

import UIKit



class PlayerTableViewCell: UITableViewCell {
    @IBOutlet weak var playerNo: UILabel!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var Substitute: UILabel!
    var player : Player?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
