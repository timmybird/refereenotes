//
//  TeamTableViewController.swift
//  Referee Notes
//
//  Created by Bartek Svaberg on 2016-04-25.
//  Copyright © 2016 Bartek Svaberg. All rights reserved.
//

import UIKit

class TeamTableViewController: UITableViewController {
    
    var match : Match?
    var team : Team?
    var teamSheet : [Player]?

    @IBOutlet weak var btnSwitchTeam: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        render()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(animated: Bool) {
        render()
        self.tableView.reloadData()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (match?.players.filter({$0.team == team!}).count)!
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("playerCell", forIndexPath: indexPath) as! PlayerTableViewCell
        let player = teamSheet![indexPath.row]
        cell.player = player
        cell.playerNo.text = String(player.number)
        cell.playerName.text = player.name
        if player.sub == true {
            cell.Substitute.text = "Substitute"
        } else {
            cell.Substitute.text = ""
        }
        
        if player.captain == true {
            cell.playerName.text = player.name + " (C)"
        }

        return cell
    }
    
    func render() -> () {
        teamSheet = match!.players.filter({$0.team == self.team!})
        teamSheet?.sortInPlace({ $0.number < $1.number })
        teamSheet?.sortInPlace({ !$0.sub && $1.sub })
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "editPlayerSegue" {
            let dest = segue.destinationViewController as! EditPlayerViewController
            let cell = sender as! PlayerTableViewCell
            dest.player = cell.player
            dest.match = match
        } else if segue.identifier == "editTeamSegue" {
            let dest = segue.destinationViewController as! EditTeamViewController
            dest.match = match
            dest.team = team
        }
    }
    
    override func viewDidAppear(animated: Bool) {
            self.title = team?.name
    }
    
    @IBAction func onBtnSwitchTeam(sender: AnyObject) {
        if self.team! == (match?.homeTeam)! {
            self.team = match?.awayTeam
        } else {
            self.team = match?.homeTeam
        }
        render()
    }
    

}
