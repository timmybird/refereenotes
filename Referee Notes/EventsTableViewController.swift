//
//  EventsTableViewController.swift
//  Referee Notes
//
//  Created by Bartek Svaberg on 2016-04-17.
//  Copyright © 2016 Bartek Svaberg. All rights reserved.
//

import UIKit

class EventsTableViewController: UITableViewController {
    
    var match : Match?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Events"

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (match?.events.count)!
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("eventCell", forIndexPath: indexPath) as! EventTableViewCell
        
        match?.events.sortInPlace({$0.time < $1.time})
        
        if let event = match?.events[indexPath.row] {
            cell.playerName.text = event.player.name
            cell.teamName.text = event.player.team.name
            cell.eventTime.text = timeAsString((match?.events[indexPath.row].time)!)
            cell.event = event
            cell.eventImage.backgroundColor = event.player.team.color
            if event.type == GOAL {
                cell.eventImage.image = UIImage(named: "Goal Small 2")
            }
            if event.type == CAUTION {
                cell.eventImage.image = UIImage(named: "Caution Small Icon")
            }
            if event.type == SENDOFF {
                cell.eventImage.image = UIImage(named: "Sendoff Small 2")
            }
            if event.type == SUBIN || event.type == SUBOUT {
                cell.eventImage.image = UIImage(named: "Substitution Small Icon")
            }
        }
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "editEventSegue" {
            if let cell = sender as? EventTableViewCell {
                let dest = segue.destinationViewController as! EditEventViewController
                if let e = cell.event {
                    dest.event = e
                }
                dest.match = match
                dest.event = cell.event
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        self.tableView.reloadData()
    }
}
