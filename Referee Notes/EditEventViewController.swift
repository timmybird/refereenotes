//
//  EditEventViewController.swift
//  Referee Notes
//
//  Created by Bartek Svaberg on 2016-04-19.
//  Copyright © 2016 Bartek Svaberg. All rights reserved.
//

import UIKit

class EditEventViewController: UIViewController {
    
    var match : Match?
    var event : Event?
    var team : Team?

    @IBAction func onTeamSwitch(sender: AnyObject) {
        if teamSwitch.on {
            teamName.text = match?.awayTeam.name
        } else {
            teamName.text = match?.homeTeam.name
        }
    }
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var player: UITextField!
    @IBOutlet weak var minutes: UITextField!
    @IBOutlet weak var seconds: UITextField!
    @IBOutlet weak var teamSwitch: UISwitch!
    @IBOutlet weak var selectEventSegmentedPicker: UISegmentedControl!
    @IBOutlet weak var eventSegmentedPicker: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    self.title = "Edit event"
        teamSwitch.tintColor = match?.homeTeam.color
        teamSwitch.onTintColor = match?.awayTeam.color
        self.team = self.event!.player.team
        teamSwitch.on = self.team?.name == match?.awayTeam.name
        player.text = String(event!.player.number)
        var mins = String(convertToMinutesAndSeconds((event?.time)!).0)
        
        if mins.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) < 2 {
            mins = "0"+mins
        }
        var secs = String(convertToMinutesAndSeconds((event?.time)!).1)
        
        if secs.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) < 2 {
            secs = "0"+secs
        }
        
        minutes.text = String(mins)
        seconds.text = String(secs)
        teamName.text = event!.player.team.name
        
        if event?.type == GOAL {
            eventSegmentedPicker.selectedSegmentIndex = 0
        } else if event?.type == SUBIN||event?.type == SUBOUT {
            eventSegmentedPicker.selectedSegmentIndex = 1
        } else if event?.type == CAUTION {
            eventSegmentedPicker.selectedSegmentIndex = 2
        } else if event?.type == SENDOFF {
            eventSegmentedPicker.selectedSegmentIndex = 3
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func onSave(sender: AnyObject) {
        
        match?.deleteEvent(event!)
        
        var newTeam : Team?
        var newPlayer : Player?
        var newTime : Double?
        var newType : String?
        
        if teamSwitch.on {
            newTeam = (match?.awayTeam)!
            
        } else {
            newTeam = (match?.homeTeam)!
        }
        
        let number: Int? = Int(player.text!)
        for p in match!.players {
            if p.team == newTeam! && p.number == number{
                newPlayer = p
            }
        }
        if let mins = Double(minutes.text!), secs = Double(seconds.text!) {
            newTime = (mins * 60) + secs
        }
        switch selectEventSegmentedPicker.selectedSegmentIndex {
        case 0: newType = GOAL
        case 1: newType = SUBIN
        case 2: newType = CAUTION
        case 3: newType = SENDOFF
        default: print("Should not happen")
        }
        if let newPlayer = newPlayer {
            if let newType = newType {
                if let newTime = newTime {
                match?.addEvent(newType, player: newPlayer, eventTime: newTime )
                }
            }
        }
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onDelete(sender: AnyObject) {
        match?.deleteEvent(event!)
        navigationController?.popViewControllerAnimated(true)
    }
    
}
