//
//  GameViewController.swift
//  Referee Notes
//
//  Created by Bartek Svaberg on 2016-04-14.
//  Copyright © 2016 Bartek Svaberg. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {
    @IBOutlet weak var homeTeamName: UIButton!
    @IBOutlet weak var awayTeamName: UIButton!
    @IBOutlet weak var awayTeamScore: UILabel!
    @IBOutlet weak var homeTeamScore: UILabel!
    @IBOutlet weak var homeTeamButton: UIButton!
    @IBOutlet weak var awayTeamButton: UIButton!
    @IBOutlet weak var time: UIButton!
    
    
    var match = Match.init(halfLength: 45*60)
    
    func initWithHardcodedStuff () {
        match.addPlayer("Mange Målvakt", number: 1, team: match.homeTeam)
        match.addPlayer("Valle Vänsterback", number: 2, team: match.homeTeam)
        match.addPlayer("Måns Mittback", number: 3, team: match.homeTeam)
        match.addPlayer("Kalle Kula", number: 4, team: match.homeTeam)
        match.addPlayer("Hasse Högerback", number: 5, team: match.homeTeam)
        match.addPlayer("Krille Kantspringare", number: 6, team: match.homeTeam)
        match.addPlayer("Mehmet Mittgeneral", number: 7, team: match.homeTeam)
        match.addPlayer("Toffe Turbo", number: 8, team: match.homeTeam)
        match.addPlayer("Bosse Bus", number: 9, team: match.homeTeam)
        match.addPlayer("Tagge Target", number: 10, team: match.homeTeam)
        match.addPlayer("Micke Målspruta", number: 11, team: match.homeTeam)
        match.addPlayer("Benke Bänknötare", number: 12, team: match.homeTeam)
        match.addPlayer("Rocky Reserv", number: 13, team: match.homeTeam)
        match.addPlayer("Jocke Junior", number: 14, team: match.homeTeam)
        match.addPlayer("Janne Jordfräs", number: 15, team: match.homeTeam)
        
        match.players[11].sub = true
        match.players[12].sub = true
        match.players[13].sub = true
        match.players[14].sub = true
        
        match.players[8].captain = true
        
        match.addPlayer("Anders Andersson", number: 1, team: match.awayTeam)
        match.addPlayer("Berra Bertilsson", number: 2, team: match.awayTeam)
        match.addPlayer("Calle Carlsson", number: 3, team: match.awayTeam)
        match.addPlayer("Danne Danielsson", number: 4, team: match.awayTeam)
        match.addPlayer("Erik Eriksson", number: 5, team: match.awayTeam)
        match.addPlayer("Frasse Fransson", number: 6, team: match.awayTeam)
        match.addPlayer("Gurra Gustavsson", number: 7, team: match.awayTeam)
        match.addPlayer("Henke Henriksson", number: 8, team: match.awayTeam)
        match.addPlayer("Ivar Ivarsson", number: 9, team: match.awayTeam)
        match.addPlayer("Janne Josefsson", number: 10, team: match.awayTeam)
        match.addPlayer("Kris Kristoferson", number: 11, team: match.awayTeam)
        match.addPlayer("Lasse Larsson", number: 12, team: match.awayTeam)
        match.addPlayer("Manne Manson", number: 13, team: match.awayTeam)
        match.addPlayer("Nicke Nicklasson", number: 14, team: match.awayTeam)
        match.addPlayer("Olle Olsson", number: 15, team: match.awayTeam)
        
        match.players[16].sub = true
        match.players[17].sub = true
        match.players[28].sub = true
        match.players[29].sub = true
        
        match.homeTeam.color = UIColor.redColor()
        match.awayTeam.color = UIColor.blueColor()
        homeTeamButton.backgroundColor = match.homeTeam.color
        awayTeamButton.backgroundColor = match.awayTeam.color
        
    }

    @IBAction func onTime(sender: AnyObject) {
        if !match.timeRunning {
            match.startHalf()
            render()
        } else if match.timeRunning {
            match.endHalf()
            render()
            
        }
    }
    
    @IBAction func onHomeAddEvent(sender: AnyObject) {
    }
    
    @IBAction func onAwayAddEvent(sender: AnyObject) {
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Match"
        initWithHardcodedStuff()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func render() -> () {
        homeTeamName.setTitle(match.homeTeam.name, forState: .Normal)
        awayTeamName.setTitle(match.awayTeam.name, forState: .Normal)
        homeTeamScore.text = String(match.getScore().0)
        awayTeamScore.text = String(match.getScore().1)
        if !match.timeRunning {
            time.setTitle("Start Half", forState: .Normal)
        } else if match.timeRunning {
            time.setTitle("End  Half", forState: .Normal)
        }
        homeTeamButton.backgroundColor = match.homeTeam.color
        awayTeamButton.backgroundColor = match.awayTeam.color
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "addHomeTeamEventSegue" {
            let dest = segue.destinationViewController as! SelectEventTypeViewController
            dest.team = match.homeTeam
            dest.match = match
        }
        if segue.identifier == "addAwayTeamEventSegue" {
            let dest = segue.destinationViewController as! SelectEventTypeViewController
            dest.team = match.awayTeam
            dest.match = match
        }
        if segue.identifier == "viewEventsSegue" {
            let dest = segue.destinationViewController as! EventsTableViewController
            dest.match = match
        }
        
        if segue.identifier == "homeTeamEditSegue" {
            let dest = segue.destinationViewController as! TeamTableViewController
            dest.match = match
            dest.team = match.homeTeam
        }
        if segue.identifier == "awayTeamEditSegue" {
            let dest = segue.destinationViewController as! TeamTableViewController
            dest.match = match
            dest.team = match.awayTeam
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        render()
    }
}
