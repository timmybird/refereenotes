//
//  Engine.swift
//  Referee Notes
//
//  Created by Bartek Svaberg on 2016-04-14.
//  Copyright © 2016 Bartek Svaberg. All rights reserved.
//

import Foundation
import UIKit

// Consider replacing these with Enums
let GOAL = "goal"
let CAUTION = "caution"
let SENDOFF = "sendOff"
let SUBIN = "subIn"
let SUBOUT = "subOut"

class Player {
    var name : String
    var number : Int
    var team : Team
    var sub : Bool
    var cautions : Int
    var sentOff : Bool
    var captain : Bool
    let id : String
    
    init (name: String , number: Int, team: Team) {
        self.name = name
        self.number = number
        self.team = team
        self.sub = false
        self.cautions = 0
        self.sentOff = false
        self.captain = false
        self.id = NSUUID().UUIDString
    }
}

func ==(lhs:Player, rhs:Player) -> Bool {
    return lhs.id == rhs.id
}


class Team {
    var name : String
    var color : UIColor
    let id : String
    
    init (name: String) {
        self.name = name
        self.color = UIColor.whiteColor()
        self.id = NSUUID().UUIDString
    }
}

func ==(lhs:Team, rhs:Team) -> Bool {
    return lhs.id == rhs.id
}

class Event : Equatable {
    var type : String
    var player : Player
    var time : Double
    let id : String
    
    init (type: String, player: Player, time: Double) {
        self.type = type
        self.player = player
        self.time = time
        id = NSUUID().UUIDString
    }
}

func ==(lhs:Event, rhs:Event) -> Bool {
    return lhs.id == rhs.id
}

class Match {
    var events : [Event]
    var halfLength : Double
    var startTime : Double
    var period : Int
    var players : [Player]
    var homeTeam : Team
    var awayTeam : Team
    var timeRunning : Bool
    
    init (halfLength : Double) {
        self.halfLength = halfLength
        self.startTime = 0
        self.events = []
        self.period = 0
        self.players = []
        self.timeRunning = false
        
        homeTeam = Team.init(name: "Home")
        awayTeam = Team.init(name: "Away")
    }
    
    func startHalf() -> () {
        self.period = self.period+1
        self.startTime = NSDate().timeIntervalSince1970
        self.timeRunning = true
    }
    
    func endHalf() -> () {
        self.timeRunning = false
    }
    
    func addEvent (type: String, player: Player) {
        var eventTime : Double
        if elapsedTime() > (self.halfLength * (Double(self.period))) {
            eventTime = (self.halfLength * (Double(self.period)))
        } else {
            eventTime = elapsedTime()
        }
        addEvent(type, player: player, eventTime: eventTime)
        
    }
    
    func addEvent(type: String, player: Player, eventTime: Double) -> () {
        let newEvent = Event.init(type: type, player: player, time: eventTime)
        if newEvent.type == SUBOUT || newEvent.type == SUBIN {
            player.sub = !player.sub
        }
        if newEvent.type == CAUTION {
            player.cautions = player.cautions+1
        }
        if newEvent.type == SENDOFF {
            player.sentOff = true
        }
        self.events.append(newEvent)
    }
    
    func editEvent (event : Event, type : String, player : Player, time : Double) {
        event.type = type
        event.player = player
        event.time = time
    }
    
    func deleteEvent (event : Event) {
        if event.type == SUBOUT || event.type == SUBIN {
            event.player.sub = !event.player.sub
        }
        if event.type == CAUTION {
            event.player.cautions = event.player.cautions-1
        }
        if event.type == SENDOFF {
            event.player.sentOff = false
        }
        if let i = events.indexOf({ e in e == event }) {
            events.removeAtIndex(i)
        }
        else {
            print("Invalid i. Event not deleted")
        }
    }
    
    func addPlayer (player: Player) {
        self.players.append(player);
    }
    
    func addPlayer (name: String, number : Int, team : Team) {
        addPlayer(Player.init(name: name, number: number, team: team))
    }
    
    func elapsedTime() -> Double {
        return (self.halfLength * (Double(self.period-1))) + (NSDate().timeIntervalSince1970-self.startTime)
    }
    
    func getScore() -> (Int, Int) {
        var score : (Int, Int) = (0, 0)
        for event in events.filter({ e in e.type == GOAL }) {
            if event.player.team.name == homeTeam.name {
                score.0 = score.0+1
            }
            if event.player.team.name == awayTeam.name {
                score.1 = score.1+1
            }
        }
        return score
    }
    
    func setCaptain(player : Player) -> () {
        for member in players {
            if member == player {
                member.captain = false
            }
        }
        player.captain = true
    }
    
    func deletePlayer(player : Player) {
        let eventsToBeDeleted = self.events.filter({$0.player == player})
        for event in eventsToBeDeleted {
            deleteEvent(event)
        }
        if let i = self.players.indexOf({ p in p == player }) {
            self.players.removeAtIndex(i)
        }
    }
    
    func getPlayer(team: Team, number : Int) -> Player? {
        return self.players.filter({ $0.team == team && $0.number == number }).first
    }
}

func convertToMinutesAndSeconds(time : Double) -> (Int, Int) {
    let minutes = time / 60
    let seconds = time % 60
    let time : (Int, Int) = (Int(minutes), Int(seconds))
    return time
}

func timeAsString(time: Double) -> String {
    let returnvalue = convertToMinutesAndSeconds(time)
    var minutes = String(returnvalue.0)
    var seconds = String(returnvalue.1)
    
    if minutes.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) < 2 {
        minutes = "0"+minutes
    }
    if seconds.lengthOfBytesUsingEncoding(NSASCIIStringEncoding) < 2 {
        seconds = "0"+seconds
    }
    return minutes+":"+seconds
}








