//
//  SelectEventPlayerViewController.swift
//  Referee Notes
//
//  Created by Bartek Svaberg on 2016-04-15.
//  Copyright © 2016 Bartek Svaberg. All rights reserved.
//

import UIKit


class SelectEventPlayerViewController: UIViewController {
    var team : Team?
    var eventType : String?
    var match: Match?
    @IBOutlet weak var inputPlayerField: UITextField!
    @IBOutlet weak var playerNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Player"
        inputPlayerField.becomeFirstResponder()
        inputPlayerField.addTarget(self, action: #selector(SelectEventPlayerViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
    }
    
    func textFieldDidChange(textField: UITextField) {
        var player: Player?
        let number = Int(inputPlayerField.text!)
        for p in match!.players {
            if p.team.name == team!.name && p.number == number{ player = p }
            playerNameLabel.text = player?.name
            //var p = match?.players.filter ({$0.team == team! && $0.number == number}).first
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onOk(sender: AnyObject) {
        
        // This method could use some cleaning up by use of getPlayer method in match.
        var player : Player?
        if let number = Int(inputPlayerField.text!) {

            player = match?.players.filter ({$0.team == team! && $0.number == Int(self.inputPlayerField.text!)}).first // Seems to work just fine.
            
            if (player == nil) {
                
                let alertView = UIAlertController(title: "No Player", message: "Player \(Int(inputPlayerField.text!)!) does not exist in \(self.team!.name). Add now?", preferredStyle: .Alert)
                //let nameTextField = alertView.textFields![0] as UITextField
                
                let addPlayer = UIAlertAction(title: "Add", style: .Default) {(_) in
                    self.match?.addPlayer("No Name", number: Int(self.inputPlayerField.text!)!, team: self.team!);
                    self.match?.addEvent(self.eventType!, player: (self.match?.players.filter ({$0.team == self.team! && $0.number == Int(self.inputPlayerField.text!)}).first!)!);
                    self.navigationController?.popToRootViewControllerAnimated(true)}
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
                alertView.addAction(addPlayer)
                alertView.addAction(cancelAction)
                
                presentViewController(alertView, animated: true, completion: nil)
 
            } else {
                match!.addEvent(eventType!, player: player!)
                self.navigationController?.popToRootViewControllerAnimated(true)
            }   
        }
        else {
            shakeElement(inputPlayerField)
        }
    }
}
