//
//  substitutionViewController.swift
//  Referee Notes
//
//  Created by Bartek Svaberg on 2016-04-15.
//  Copyright © 2016 Bartek Svaberg. All rights reserved.
//

import UIKit

class substitutionViewController: UIViewController {
    var team: Team? = nil
    var match: Match? = nil

    @IBOutlet weak var playerInInput: UITextField!
    @IBOutlet weak var playerOutInput: UITextField!
    @IBOutlet weak var lblPlayerOne: UILabel!
    @IBOutlet weak var lblPlayerTwo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Substitution Players"
        lblPlayerOne.text = "No Player"
        lblPlayerTwo.text = "No Player"
        
        playerInInput.becomeFirstResponder()
        playerInInput.addTarget(self, action: #selector(substitutionViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        
        playerOutInput.addTarget(self, action: #selector(substitutionViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)

        // Do any additional setup after loading the view.
    }
    
    func textFieldDidChange(textField: UITextField) {
        var player : Player?
        var output : UILabel?
        if let _ : Int = Int(textField.text!) {
            player = (match?.getPlayer(team!, number: Int(textField.text!)!))
        }
        if textField == playerInInput {
            output = lblPlayerOne
        } else if textField == playerOutInput {
            output = lblPlayerTwo
        }
        if player != nil {
            output!.text = player!.name
        } else {
            output!.text = "No Player"
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    @IBAction func onOk(sender: AnyObject) {
        
        if let _ : Int = Int(playerInInput.text!) {
            if let _ : Int = Int(playerOutInput.text!) {
                
                let player1 : Player? = match?.getPlayer(team!, number: Int(playerInInput.text!)!)
                let player2 : Player? = match?.getPlayer(team!, number: Int(playerOutInput.text!)!)
                
                if player1 == nil && player2 == nil {
                    let alertView = UIAlertController(title: "No Player", message: "Players \(Int(playerInInput.text!)!) and \(Int(playerOutInput.text!)!) does not exist in \(self.team!.name). Add now?", preferredStyle: .Alert)
                    
                    let addPlayer = UIAlertAction(title: "Add", style: .Default) {(_) in
                        self.match?.addPlayer("No Name", number: Int(self.playerInInput.text!)!, team: self.team!);
                        self.match?.addPlayer("No Name", number: Int(self.playerOutInput.text!)!, team: self.team!);
                        self.match?.addEvent(SUBIN, player: (self.match?.getPlayer(self.team!, number: Int(self.playerInInput.text!)!))!);
                        self.match?.addEvent(SUBOUT, player: (self.match?.getPlayer(self.team!, number: Int(self.playerInInput.text!)!))!);
                        self.navigationController?.popToRootViewControllerAnimated(true)}
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
                    alertView.addAction(addPlayer)
                    alertView.addAction(cancelAction)
                    
                    presentViewController(alertView, animated: true, completion: nil)
                    
                } else if player1 == nil {
                    let alertView = UIAlertController(title: "No Player", message: "Player \(Int(playerInInput.text!)!) does not exist in \(self.team!.name). Add now?", preferredStyle: .Alert)
                    
                    let addPlayer = UIAlertAction(title: "Add", style: .Default) {(_) in
                        self.match?.addPlayer("No Name", number: Int(self.playerInInput.text!)!, team: self.team!);
                        self.match?.addEvent(SUBIN, player: (self.match?.getPlayer(self.team!, number: Int(self.playerInInput.text!)!))!);
                        self.match?.addEvent(SUBOUT, player: (self.match?.getPlayer(self.team!, number: Int(self.playerOutInput.text!)!))!);
                        self.navigationController?.popToRootViewControllerAnimated(true)}
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
                    alertView.addAction(addPlayer)
                    alertView.addAction(cancelAction)
                    
                    presentViewController(alertView, animated: true, completion: nil)
                    
                } else if player2 == nil {
                    
                    let alertView = UIAlertController(title: "No Player", message: "Player \(Int(playerOutInput.text!)!) does not exist in \(self.team!.name). Add now?", preferredStyle: .Alert)
                    
                    let addPlayer = UIAlertAction(title: "Add", style: .Default) {(_) in
                        self.match?.addPlayer("No Name", number: Int(self.playerOutInput.text!)!, team: self.team!);
                        self.match?.addEvent(SUBIN, player: (self.match?.getPlayer(self.team!, number: Int(self.playerInInput.text!)!))!);
                        self.match?.addEvent(SUBOUT, player: (self.match?.getPlayer(self.team!, number: Int(self.playerOutInput.text!)!))!);
                        self.navigationController?.popToRootViewControllerAnimated(true)}
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
                    alertView.addAction(addPlayer)
                    alertView.addAction(cancelAction)
                    
                    presentViewController(alertView, animated: true, completion: nil)
                    
                } else {
                    match!.addEvent(SUBIN, player: player1!)
                    match?.addEvent(SUBOUT, player: player2!)
                    self.navigationController?.popToRootViewControllerAnimated(true)
                }
            } else {
                shakeElement(playerOutInput)
            }
        } else {
            shakeElement(playerInInput)
        }
    }
}
