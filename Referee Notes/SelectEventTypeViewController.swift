//
//  SelectEventTypeViewController.swift
//  Referee Notes
//
//  Created by Bartek Svaberg on 2016-04-14.
//  Copyright © 2016 Bartek Svaberg. All rights reserved.
//

import UIKit

class SelectEventTypeViewController: UIViewController {
    var team: Team?
    var match: Match?
    @IBOutlet weak var teamNameLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Events"
        teamNameLabel.text = team!.name

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "addGoalEventSegue" {
            let dest = segue.destinationViewController as! SelectEventPlayerViewController
            dest.team = self.team
            dest.eventType = GOAL
            dest.match = match
        }
        if segue.identifier == "addYellowCardEventSegue" {
            let dest = segue.destinationViewController as! SelectEventPlayerViewController
            dest.team = self.team
            dest.eventType = CAUTION
            dest.match = match
        }
        if segue.identifier == "addRedCardEventSegue" {
            let dest = segue.destinationViewController as! SelectEventPlayerViewController
            dest.team = self.team
            dest.eventType = SENDOFF
            dest.match = match
        }
        if segue.identifier == "addSubstitutionEventSegue" {
            let dest = segue.destinationViewController as! substitutionViewController
            dest.team = self.team
            dest.match = match
        }
    }
}
