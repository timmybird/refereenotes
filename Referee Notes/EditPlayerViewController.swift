//
//  EditPlayerViewController.swift
//  Referee Notes
//
//  Created by Bartek Svaberg on 2016-04-26.
//  Copyright © 2016 Bartek Svaberg. All rights reserved.
//

import UIKit



class EditPlayerViewController: UIViewController {
    @IBOutlet weak var tfPlayerName: UITextField!
    @IBOutlet weak var tfPlayerNumber: UITextField!
    @IBOutlet weak var switchIsCaptain: UISwitch!
    @IBOutlet weak var switchIsSub: UISwitch!

    var player : Player?
    var match : Match?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Edit Player"
        render()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSave(sender: AnyObject) {
        if let _ : Int = Int(tfPlayerNumber.text!) {
            if tfPlayerName.text == "" {
                player?.name = ""
            } else {
                player!.name = tfPlayerName.text!
            }
            player!.number = Int(tfPlayerNumber.text!)!
            if switchIsCaptain.on {
                match!.setCaptain(player!)
            }
            if switchIsSub.on {
                player?.sub = true
            } else {
                player?.sub = false
            }
            navigationController?.popViewControllerAnimated(true)
        } else {
            shakeElement(tfPlayerNumber)
        }
    }

    @IBAction func onCancel(sender: AnyObject) {
        render()
        navigationController?.popViewControllerAnimated(true)
    }
    
    func render () -> () {
        tfPlayerName.text = player?.name
        tfPlayerNumber.text = String(player!.number)
        switchIsCaptain.on = (player?.captain)!
        switchIsSub.on = (player?.sub)!
    }

    @IBAction func onDeletePlayer(sender: AnyObject) {
        match!.deletePlayer(player!)
        navigationController?.popViewControllerAnimated(true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
